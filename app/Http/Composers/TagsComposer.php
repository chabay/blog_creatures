<?php
//  ./app/Http/Composers/TagsComposer.php

namespace App\Http\Composers;

use Illuminate\View\View;
use \App\Http\Models\Tag;

class TagsComposer {
    /**
     * @param  View  $view
     * @return void
     */
    public function compose(View $view) {
        $view->with('tags', Tag::all());
    }

    /**
     * @param  View  $view
     * @return void
     */
    public function index(View $view) {
        $view->with('tags', Tag::all());
    }
}
