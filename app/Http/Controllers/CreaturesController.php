<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Http\Models\Creature;

class CreaturesController extends Controller
{
  /**
   * Affiche le détail d'une créature
   * @param  Creature $creature
   * @return vue creatures.show
   */
  public function show(Creature $creature) {
    return View::make('creatures.show', compact('creature'));
  }

  /**
   * Recherche sur les créatures
   * @param  Request $request
   * @return vue creatures.search
   */
  public function search(Request $request) {
    $search = request()->input('search');
    $creatures = Creature::where('nom', 'like', '%' . "%$search%")->get();
    return View::make('creatures.search', compact('creatures'));

  }

}
