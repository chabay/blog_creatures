<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use App\Http\Models\Film;

class FilmsController extends Controller
{
  /**
   * Affiche le détail d'un film
   * @param  Film   $film
   * @return vue films.show
   */
   public function show(Film $film) {
     return View::make('films.show', compact('film'));
   }

  /**
   * Affiche le formulaire d'ajout d'un film
   * @return vue films.create
   */
   public function create() {
     return View::make('films.create');
   }

   /**
    * Insert du film dans la db
    * @param  Request $request
    * @return redirection vers la liste des films
    */
   public function store(Request $request) {
     Film::create($request->all());
     return redirect()->route('pages.show', ['page' => 3,
                                             'slug' => 'les-films']);
   }

   /**
    * Affiche le formulaire de modification d'un film
    * @param  Film   $film
    * @return vue films.edit
    */
   public function edit(Film $film) {
     return View::make('films.edit', compact('film'));
   }
   /**
    * Update du film dans la db
    * @param  Request $request
    * @param  Film    $film
    * @return redirection vers le détail du film
    */
   public function update(Request $request, Film $film) {
     $film->update($request->all());
     return redirect()->route('films.show', ['film' => $film->id,
                                             'slug' => Str::slug($film->titre)]);
   }

   /**
    * Suppression d'un film de la db
    * @param  Film   $film
    * @return redirection vers la liste des films
    */
   public function destroy(Film $film) {
     $film->destroy($film->id);
     return redirect()->route('pages.show', ['page' => 3,
                                             'slug' => 'les-films']);
   }
}
