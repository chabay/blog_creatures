<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Http\Models\Page;
use App\Http\Models\Film;
use App\Http\Models\Creature;

class PagesController extends Controller
{
 /**
  * Affiche le détail d'une page
  * @param  Page   $page
  * @return vue pages.show
  */
  public function show(Page $page) {
    if ($page->id ===1):
      $creatures = Creature::orderBy('created_at', 'DESC')->take(2)->get();
      return View::make('pages.show', compact('page', 'creatures'));
    elseif ($page->id === 2):
      $creatures = Creature::all();
      return View::make('pages.show', compact('page', 'creatures'));
    elseif ($page->id === 3):
      $films = Film::all();
      return View::make('pages.show', compact('page', 'films'));
    endif;
    return View::make('pages.show', compact('page'));
  }

}
