<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Creature extends Model
{
  /**
   * Va chercher le film qui contient la créature
   * @return [type] [description]
   */
  public function filmData() {
    return $this->belongsTo('App\Http\Models\Film', 'film');
  }

  /**
   * Va chercher les tags de la créature
   * @return [type] [description]
   */
  public function tags() {
    return $this->belongsToMany('App\Http\Models\Tag', 'creatures_has_tags', 'creature', 'tag');
  }
}
