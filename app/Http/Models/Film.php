<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
  protected $fillable = ['titre', 'synopsis'];

  /**
   * Va chercher les créatures du film
   * @return [type] [description]
   */
  public function creatures() {
    return $this->hasMany('App\Http\Models\Creature', 'film');
  }

}
