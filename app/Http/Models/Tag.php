<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{

  /**
   * Va chercher les créatures du tag
   * @return [type] [description]
   */
  public function creatures() {
    return $this->belongsToMany('App\Http\Models\Creature', 'creatures_has_tags', 'tag', 'creature');
  }

}
