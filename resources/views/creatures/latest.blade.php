{{--
  ./resources/views/creatures/latest.blade.php
  Variables disponibles :
      - $creatures array(Creature)
 --}}

<!-- Title -->
<h2 class="mt-4">Dernieres créatures</h2>

<hr>
@foreach ($creatures as $creature)
<!-- Project One -->
<div class="row">
  <div class="col-md-4">
    <a href="#">
      <img class="img-fluid rounded mb-3 mb-md-0" src="{{ asset('images/' . $creature->image)}}" alt="">
    </a>
  </div>
  <div class="col-md-8">
    <h3>{{ $creature->nom }}</h3>
    <p class="lead">
      dans
      <a href="{{ route('films.show', [
        'film' => $creature->filmData->id,
        'slug' => Str::slug($creature->filmData->titre)
        ])}}">{{ $creature->filmData->titre }}</a>
      le {{ \Carbon\Carbon::parse($creature->created_at)->format('d/m/Y') }}
    </p>
    <p>{{ $creature->texteLead }}</p>

    <div>
      <a class="btn btn-primary" href="{{ route('creatures.show', [
          'creature' => $creature->id,
          'slug' => Str::slug($creature->nom)
        ]) }}">Voir la créature</a>

    </div>
    <hr/>
    <ul class="list-inline tags">
      @foreach($creature->tags as $tag)
      <li><a href="{{ route('tags.show', [
          'tag' => $tag->id,
          'slug' => Str::slug($tag->nom)
        ]) }}" class="btn btn-default btn-xs">{{ $tag->nom }}</a></li>
      @endforeach
    </ul>
  </div>
</div>
<!-- /.row -->

<hr>
@endforeach
