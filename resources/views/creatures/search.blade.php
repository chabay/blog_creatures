{{--
  ./resources/views/creatures/searchresults.blade.php
  Variables disponibles :
      - $creatures array(Creature)
 --}}

 @extends('template.defaut')

 @section('title')
   Résultats de la recherche : {{ request()->input('search') }}
 @endsection

 @section('content1')

   <!-- Page Heading -->

   <!-- Title -->
   <h1 class="mt-4">Résultats de la recherche : <small>{{ request()->input('search') }}</small></h1>

   <hr>

   @foreach ($creatures as $creature)
     <!-- Project One -->
     <div class="row">
       <div class="col-md-4">
         <a href="#">
           <img class="img-fluid rounded mb-3 mb-md-0" src="{{ asset('images/' . $creature->image)}}" alt="">
         </a>
       </div>
       <div class="col-md-8">
         <h3>{{ $creature->nom }}</h3>
         <p class="lead">
           dans
           <a href="{{ route('films.show', [
             'film' => $creature->filmData->id,
             'slug' => Str::slug($creature->filmData->titre)
             ])}}">{{ $creature->filmData->titre }}</a>
           le {{ \Carbon\Carbon::parse($creature->created_at)->format('d/m/Y') }}
         </p>
         <p>{{ $creature->texteLead }}</p>

         <div>
           <a class="btn btn-primary" href="{{ route('creatures.show', [
               'creature' => $creature->id,
               'slug' => Str::slug($creature->nom)
             ]) }}">Voir la créature</a>

         </div>
         <hr/>
         <ul class="list-inline tags">
           @foreach($creature->tags as $tag)
           <li><a href="#" class="btn btn-default btn-xs">{{ $tag->nom }}</a></li>
           @endforeach
         </ul>
       </div>
     </div>
     <!-- /.row -->

     <hr>
   @endforeach
 @endsection
