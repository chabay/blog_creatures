{{--
  ./resources/views/creatures/show.blade.php
  - Variables disponibles
    $creature Creature
 --}}

@extends('template.defaut')

@section('title')
  {{ $creature->nom }}
@endsection

@section('content1')

  <!-- Page Heading -->

  <!-- Title -->
  <h1 class="mt-4">{{ $creature->nom }}</h1>
  <p class="lead">
    dans
    <a href="{{ route('films.show', [
      'film' => $creature->filmData->id,
      'slug' => Str::slug($creature->filmData->titre)
      ])}}">{{ $creature->filmData->titre }}</a>
    le {{ \Carbon\Carbon::parse($creature->created_at)->format('d/m/Y') }}
  </p>

  <hr>

  <!-- Project One -->
  <div class="row">
    <div class="col-md-6">
      <a href="#">
        <img class="img-fluid rounded mb-3 mb-md-0" src="{{ asset('images/' . $creature->image)}}" alt="">
      </a>
    </div>
    <div class="col-md-6">
      <p class="lead">{{ $creature->texteLead }}</p>
      <hr/>
      <p>{{ $creature->texteSuite }}</p>

      <hr/>
      <ul class="list-inline tags">
        @foreach($creature->tags as $tag)
        <li><a href="{{ route('tags.show', [
            'tag' => $tag->id,
            'slug' => Str::slug($tag->nom)
          ]) }}" class="btn btn-default btn-xs">{{ $tag->nom }}</a></li>
        @endforeach
      </ul>
    </div>
  </div>
  <!-- /.row -->
  <hr>

@endsection
