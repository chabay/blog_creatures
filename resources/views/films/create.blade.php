{{--
  ./resources/views/films/create.blade.php
  - Variables disponibles
    /
 --}}

@extends('template.defaut')

@section('title')
  Ajouter un film
@endsection

@section('content1')
<h1 class="mt-4">Ajouter un Film</h1>

<form action="{{ route('films.store') }}" method="POST">
    @csrf
  <div class="form-group">
    <label for="titre">Titre</label>
    <input name="titre" type="text" class="form-control" />
  </div>
  <div class="form-group">
    <label for="synopsis">Synopsis</label>
    <textarea name="synopsis" rows="8" cols="80" class="form-control"></textarea>
  </div>
  <div class="form-group">
    <input type="submit" class="btn btn-primary" />
  </div>
</form>

<!-- /.row -->
<hr>
@endsection
