{{--
  ./resources/views/films/edit.blade.php
  - Variables disponibles
    $film Film
 --}}

 @extends('template.defaut')

 @section('title')
   Modifier un film
 @endsection

 @section('content1')

   <h1 class="mt-4">Modifier un Film</h1>

   <form action="{{ route('films.update', [
       'film' => $film->id,
       'slug' => Str::slug($film->titre)
     ]) }}" method="POST">
       @csrf
       {{ method_field('PATCH') }}
     <div class="form-group">
       <label for="titre">Titre</label>
       <input name="titre" type="text" class="form-control" value="{{ $film->titre }}"/>
     </div>
     <div class="form-group">
       <label for="synopsis">Synopsis</label>
       <textarea name="synopsis" rows="8" cols="80" class="form-control">
         {{ $film->synopsis }}
       </textarea>
     </div>
     <div class="form-group">
       <input type="submit" class="btn btn-primary" />
     </div>
   </form>

   <!-- /.row -->
   <hr>


@endsection
