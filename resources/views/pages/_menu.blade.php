{{--
  ./resources/views/pages/_menu.blade.php
  variables disponibles :
      - $pages array(Page)
 --}}

<ul class="navbar-nav ml-auto">
  @foreach ($pages as $page)
  <li class="nav-items">
    <a class="nav-link {{ request()->segment(2) == $page->id ? 'active' : '' }}" href="{{ route('pages.show', [
        'page' => $page->id,
        'slug' => Str::slug($page->titre)
      ]) }}">
      {{ $page->titre }}
    </a>
  </li>
  @endforeach
</ul>
