{{--
  ./resources/views/pages/show.blade.php
  variables disponibles :
      - $page Page
 --}}

@extends('template.defaut')

@section('title')
  {{ $page->titre }}
@endsection

@section('content1')
<!-- Page Heading -->

<!-- Title -->
<h1 class="mt-4">{{ $page->titre }}</h1>

<hr>


<!-- Post Content -->
<p class="lead">{{ $page->texte }}</p>


<hr>


@if ($page->id === 1)

  @include('creatures.latest')

@elseif ($page->id === 2)

  @include('creatures.index')

@elseif ($page->id === 3)

  @include('films.index')

@endif


@endsection
