{{--
  ./resources/views/tags/_index.blade.php
  variables disponibles :
      - $tags array(Tag)
 --}}

<ul class="list-unstyled mb-0">
  @foreach ($tags as $tag)
  <li>
    <a href="{{ route('tags.show', [
        'tag' => $tag->id,
        'slug' => Str::slug($tag->nom)
      ]) }}">
      {{ $tag->nom }}
    </a>
  </li>
  @endforeach
</ul>
