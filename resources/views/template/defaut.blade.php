<!DOCTYPE html>
<html lang="en">

  <head>
    @include('template.partials._head')
  </head>

  <body>

    <!-- Navigation -->
  @include('template.partials._nav')

    <!-- Page Content -->
    <div class="container">

      <div class="row">

        <!-- Post Content Column -->
        <div class="col-lg-8">

          @yield('content1')

        </div>

        <!-- Sidebar Widgets Column -->
      @include('template.partials._sidebar')

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    @include('template.partials._footer')

    <!-- Scripts -->
    @include('template.partials._scripts')

  </body>

</html>
