<div class="col-md-4">

  <!-- Search Widget -->
  <div class="card my-4">
    <h5 class="card-header">Rechercher une créature</h5>
    <div class="card-body">
      <form class="" action="{{ route('creatures.search') }}" method="get">
        <div class="input-group">
          <input name="search" value="{{ request()->input('search') }}" type="text" class="form-control" placeholder="Mot-clé">
          <span class="input-group-btn">
            <button class="btn btn-secondary" type="submit">Go!</button>
          </span>
        </div>
      </form>
    </div>
  </div>

  <!-- Categories Widget -->
  <div class="card my-4">
    <h5 class="card-header">Tags</h5>
    <div class="card-body">
      <div class="row">
        <div class="col-lg-12">
          @include('tags._index')
        </div>

      </div>
    </div>
  </div>


</div>
