<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ROUTE PAR DEFAUT
// PATTERN : /
// CTRL : PagesController
// ACTION : Show

Route::get('/', 'PagesController@show')->defaults('page', 1)->name('homepage');

// ROUTE DU DETAIL D'UNE PAGE
// PATTERN : /pages/id/slug.html
// CTRL : PagesController
// ACTION : Show

Route::get('/pages/{page}/{slug}.html', 'PagesController@show')
->where(['page' => '[1-9][0-9]*',
         'slug' => '[a-z0-9]+[a-z0-9\-]*[a-z0-9]'])
->name('pages.show');

//ROUTES DES CREATURES

Route::prefix('creatures')->name('creatures.')->group(function () {

  // ROUTE DU DETAIL D'UNE CREATURE
  // PATTERN : /creatures/id/slug.html
  // CTRL : CreaturesController
  // ACTION : Show

  Route::get('{creature}/{slug}.html', 'CreaturesController@show')
  ->where(['creature' => '[1-9][0-9]*',
           'slug' => '[a-z0-9]+[a-z0-9\-]*[a-z0-9]'])
  ->name('show');

  // ROUTE DE LA RECHERCHE SUR LES CREATURES
  // PATTERN : /creatures/search.html?search=...
  // CTRL : CreaturesController
  // ACTION : Search

  Route::get('search.html', 'CreaturesController@search')
  ->name('search');
});

//ROUTES DES FILMS

Route::prefix('films')->name('films.')->group(function () {

  // ROUTE DU DETAIL D'UN FILM
  // PATTERN : /films/id/slug.html
  // CTRL : FilmsController
  // ACTION : Show

  Route::get('{film}/{slug}.html', 'FilmsController@show')
  ->where(['film' => '[1-9][0-9]*',
           'slug' => '[a-z0-9]+[a-z0-9\-]*[a-z0-9]'])
  ->name('show');

  // ROUTE DE L'AJOUT D'UN FILM
  // PATTERN : /films/add.html
  // CTRL : FilmsController
  // ACTION : Create

  Route::get('add.html', 'FilmsController@create')
  ->name('create');

  // ROUTE DE L'INSERT D'UN FILM
  // PATTERN : /films [POST]
  // CTRL : FilmsController
  // ACTION : store

  Route::post('', 'FilmsController@store')
  ->name('store');

  // ROUTE DE L'EDIT D'UN FILM
  // PATTERN : films/id/slug/edit.html
  // CTRL : FilmsController
  // ACTION : edit

  Route::get('{film}/{slug}/edit.html', 'FilmsController@edit')
  ->where(['film' => '[1-9][0-9]*',
           'slug' => '[a-z0-9]+[a-z0-9\-]*[a-z0-9]'])
  ->name('edit');

  // ROUTE DE L'UPDATE D'UN FILM
  // PATTERN : /films/id/slug.html [PATCH]
  // CTRL : FilmsController
  // ACTION : update

  Route::patch('{film}/{slug}.html', 'FilmsController@update')
  ->where(['film' => '[1-9][0-9]*',
           'slug' => '[a-z0-9]+[a-z0-9\-]*[a-z0-9]'])
  ->name('update');

  // ROUTE DE LA SUPPRESSION D'UN FILM
  // PATTERN : /films/id/slug.html  [DELETE]
  // CTRL : FilmsController
  // ACTION : destroy

  Route::delete('{film}/{slug}.html', 'FilmsController@destroy')
  ->where(['film' => '[1-9][0-9]*',
          'slug' => '[a-z0-9]+[a-z0-9\-]*[a-z0-9]'])
  ->name('destroy');

});


// ROUTE DU DETAIL D'UN TAG
// PATTERN : /tags/id/slug.html
// CTRL : TagsController
// ACTION : Show

Route::get('/tags/{tag}/{slug}.html', 'TagsController@show')
->where(['tag' => '[1-9][0-9]*',
        'slug' => '[a-z0-9]+[a-z0-9\-]*[a-z0-9]'])
->name('tags.show');
